﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using KaiserMVVM;

namespace GUI.ViewModel
{
    public class MainVM : ViewModelBase
    {
        private string textContent;
        private string userMessage;

        private readonly string contentDirectory =
            Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\MiniNotes\\";

        private const string CONTENT_FILE = "MiniNotes_Content.txt";

        public string TextContent
        {
            get => this.textContent;
            set { 
                this.textContent = value;
                this.SaveChanges(value);
            }
        }

        public MainVM()
        {
            this.TextContent = this.LoadContent();
        }

        public string UserMessage
        {
            get => this.userMessage;
            set => base.Set(ref this.userMessage, value);
        }
        
        private void SaveChanges(string value)
        {
            if (!Directory.Exists(this.contentDirectory))
                Directory.CreateDirectory(this.contentDirectory);
            File.WriteAllText(this.contentDirectory + CONTENT_FILE, value);

            Task.Run(() =>
            {
                this.UserMessage = "Changes saved!";
                Thread.Sleep(3000);
                this.UserMessage = "";
            });
        }

        private string LoadContent()
        {
            if (!File.Exists(this.contentDirectory + CONTENT_FILE))
                return string.Empty;

            return File.ReadAllText(this.contentDirectory + CONTENT_FILE);
        }
    }
}
