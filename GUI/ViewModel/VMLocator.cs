﻿using KaiserMVVM;

namespace GUI.ViewModel
{
    public class VMLocator
    {
        public VMLocator()
        {
            IocContainer.Register<MainVM>();
            IocContainer.Register<FeedbackVM>();
        }

        public MainVM MainMVInstance => IocContainer.GetInstance<MainVM>();
        public FeedbackVM FeedbackMVInstance => IocContainer.GetInstance<FeedbackVM>();
    }
}
